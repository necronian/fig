# fig

A <s>simplistic</s> Clojure library for reading configuration data using clojure.edn
Because fig sounds like config. Get it?

![It's a picture of a fig](fig.jpg)

## Installation

Add the following dependency to your `project.clj` file

[![Clojars Project](http://clojars.org/fig/latest-version.svg)](http://clojars.org/fig)

## Getting started

Create a configuration file containing [clojure.edn](https://github.com/edn-format/edn) formatted data.

``` clojure
{:configuration "My Configuration"}
```

Then in your program
``` clojure
(require '[fig.core :as config])
(config/read-config "path/to/configuration.edn")
```

## Usage

Fig reads configuration files in edn syntax while supplying useful tag literals for transforming the configuration programmatically.

## Included Tags
 `fig/bigdec`, `fig/bool`, `fig/env`, `fig/float`, `fig/include`, `fig/int`, `fig/join`, `fig/keyword`, `fig/product`, `fig/merge`, `fig/merge-into`, `fig/or`, `fig/product-tag` and `fig/ref`

### fig/bigdec
Converts the supplied value to a bigdec number.

### fig/bool
Converts a string to a bool.

### fig/env
Read environmental variables.

### fig/float
Converts the supplied string value to a float.

### fig/include
Include the contents of another configuration file at this location. It does its best to find the file to include and searches for the file in the projects resources, relative to the configuration file and finally checks if it is an absolute path.

Example, with two configuration files.

config1.edn
``` clojure
{:foo #fig/include "config2.edn"}
```
config2.edn
``` clojure
"bar"
```

clojure
``` clojure
(require '[fig.core :as config])
(config/read-config "path/to/configuration.edn")
```
Returns:
``` clojure
{:foo "bar"}
```

### fig/int
Converts the supplied value to an int.

### fig/join
Joins a collection into a single string.

### fig/keyword
Converts the supplied value to a keyword.

### fig/product and fig/product-tag
Takes a collection, the first item in the collection must be a map of collections.
The second can be any valid edn.

The Cartesian product of the map is computed and then all `#fig/product-tags` in the second collection are replaced with the computed values from the computed Cartesian product.

Example:
```clojure
#fig/product [{:foo [0 1] :bar [2 3]}
[#fig/product-tag :foo #fig/product-tag :bar]]
```
Returns
``` clojure
([0 2] [0 3] [1 2] [1 3])
```

### fig/merge
Merges the supplied values.

### fig/merge-into
Uses clojure merge-with using the into strategy to merge together the supplied values.

### fig/or
Runs clojure or on the supplied values.

### fig/ref
Refer to configuration located elsewhere in the file at this location, takes a vector of arguments and works similar to clojure's get-in function.

Example:
```clojure
{:foo "foo"
 :bar #fig/ref [:foo]}
```
Returns:
```clojure
{:foo "foo", :bar "foo"}
```

## Acknowledgments
Fig came to life after I ran into [Aero](https://github.com/juxt/aero)'s limitations with deeply nested reader tags. So I wrote fig trading off speed for flexibility.

## License

Copyright © 2018

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
