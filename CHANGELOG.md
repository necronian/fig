# Change Log
All notable changes to this project will be documented in this file. This change log follows the conventions of [keepachangelog.com](http://keepachangelog.com/).

## [0.2.0]
### Changed
- Slightly re-arranged the code for performance reasons.

## [0.1.0]
### Changed
- Initial version of the library can read and render configuration files.

[Unreleased]: https://github.com/your-name/fig/compare/0.1.0...HEAD

