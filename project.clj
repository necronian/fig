(defproject fig "0.2.0"
  :description "A Clojure library for reading configuration data using clojure.edn"
  :url "https://gitlab.com/necronian/fig"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]])
