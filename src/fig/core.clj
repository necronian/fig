(ns fig.core
  "Fig is a configuration file reader based on clojure.edn."
  (:require [clojure.edn :as edn]
            [clojure.java.io :as io :refer [file resource]]
            [clojure.string :refer [join]]
            [clojure.walk :refer [postwalk prewalk]]))

(defprotocol Transformable
  "Items implementing Transformable know when they should be run in the
  evaluation lifecyle and how to transform the input into the correct data."
  (transform [this c v]
    "Takes a configuration map and a value and returns the evaluated data.")
  (prewalkable [this] "If this is evaluated in the first pre-walk cycle.")
  (postwalkable [this] "If this is evaluated last in the post-walk cycle."))

(defn transformable?*
  "Checks if the supplied argument supports the Transformable Protocol."
  [this]
  (satisfies? Transformable this))

(def
  ^{:doc
    "Checks if the supplied argument supports the Transformable Protocol."}
  transformable?
  (memoize transformable?*))

;;; Create a record to use for later realizing functions inserted after the
;;; first configuration generation
(defrecord Tag [f step]
  Transformable
  (transform [this c v] ((.f this) c v))
  (prewalkable [this] (= :prewalk (.step this)))
  (postwalkable [this] (= :postwalk (.step this))))

(defn- walk-configuration*
  "Takes specific edn data that needs to be transformed. And a map of data to
  be used in the transformation. First the edn-data is prewalked and any
  Tags that have a :prewalk step are executed with the parent data
  supplied to them. Then the tree is postwalked and any :postwalk step
  Tags are executed with the parent-data."
  [c d p]
  (let [times (atom 0)]
    [(->> d
          (prewalk (fn [coll]
                     (if (and (transformable? coll) (prewalkable coll))
                       (do (swap! times inc) (transform coll c p))
                       coll)))
          (postwalk (fn [coll]
                      (if (and (transformable? coll) (postwalkable coll))
                        (do (swap! times inc) (transform coll c p))
                        coll))))
     @times]))

(def
  ^{:private true
    :doc "Takes specific edn data that needs to be transformed. And a map of
          data to be used in the transformation. First the edn-data is
          prewalked and any Tags that have a :prewalk step are
          executed with the parent data supplied to them. Then the tree is
          postwalked and any :postwalk step Tags are executed with
          the parent-data."}
  walk-configuration
  (memoize walk-configuration*))

(defn realize-tags*
  "Keep Walking the collection until no more transformations have been
  applied. Takes the configuration leaf to transform and optionally the
  current parsed configuration. A map of option arguments can be supplied as
  the last argument. The :max-walks key controls the maximum recursion depth
  of the tree walking."
  ([c p v & {:keys [max-walks] :or {max-walks 300}}]
   (let [walks (atom 0)]
     (cond (or (map? v) (sequential? v))
           (loop [[walked times] (walk-configuration c v p)]
             (if (or (zero? times) (< max-walks @walks))
               walked
               (recur (walk-configuration c walked p))))
           (transformable? v) (transform c v p)
           :else v))))

(def
  ^{:doc "Keep Walking the collection until no more transformations have been
          applied. Takes the configuration leaf to transform and optionally
          the current parsed configuration. A map of option arguments can be
          supplied as the last argument. The :max-walks key controls the
          maximum recursion depth of the tree walking."}
  realize-tags
  (memoize realize-tags*))

(defn- fig-bigdec
  "Converts the supplied value to a bigdec number."
  [v]
  (Tag. (fn [c p] (bigdec (realize-tags c p v))) :postwalk))

(defn- fig-bool
  "Converts a string to a bool."
  [v]
  (Tag. (fn [c p] (condp = (realize-tags c p v) "true" true "false" false))
        :postwalk))

(defn- fig-env
  "Fetch values from the system environment."
  [v]
  (Tag. (fn [c p] (System/getenv (str (realize-tags c p v)))) :postwalk))

(defn- fig-float
  "Converts a string to a float."
  [v]
  (Tag. (fn [c p] (Float. (realize-tags c p v))) :postwalk))

;;; fig-include uses the same file reader as read-config, but it has not been
;;; defined yet.
(declare reader)

(defn- fig-include
  "Include other files. Tries to check if they are absolute paths or relative
  to the base configuration directory."
  [v]
  (Tag.
   (fn [c _]
     ;; keep falling back to try to find a match
     (let [f (or (resource v)
                 (file (get c :path) v)
                 (file val))]
       (reader f)))
   :postwalk))

(defn- fig-int
  "Converts a string to an int."
  [v]
  (Tag. (fn [c p] (Integer. (realize-tags c p v))) :postwalk))

(defn- fig-join
  "Joins all the supplied values as a string."
  [v]
  (Tag. (fn [c p] (join (realize-tags c p v))) :postwalk))

(defn- fig-keyword
  "Converts the supplied value to a keyword."
  [v]
  (Tag. (fn [c p] (keyword (realize-tags c p v))) :postwalk))

;;; Ohhhh, Mathy sounding!
(defn- cartesian-product
  "Calculate the Cartesian product of the supplied collections."
  [[f & r]]
  (if (nil? (seq f))
    '(())
    (for [x f y (cartesian-product r)]
      (cons x y))))

(defn- fig-product
  "Calculate the cartesian product of the first argument and places it into
  the configuration with the namespaced keyword ::fig-product. Then the
  collection in the second argument is walked and any #fig/product-tags are
  replaced with the value looked up by their key."
  [[ks v]]
  (Tag.
   (fn [c p]
     (pmap #(realize-tags (merge c {::fig-product %}) p v)
           (map (partial zipmap (keys ks)) (cartesian-product (vals ks)))))
   :prewalk))

(defn- fig-merge
  "Merges the supplied values."
  [v]
  (Tag. (fn [c p] (apply merge (realize-tags c p v))) :postwalk))

(defn- fig-merge-into
  "Uses clojure merge-with using the into strategy to merge together the
  supplied values."
  [v]
  (Tag. (fn [c p] (apply (partial merge-with into) (realize-tags c p v)))
        :postwalk))

(defn- fig-or
  "Runs clojure or on the supplied values."
  [v]
  (Tag. (fn [c p] (eval `(or ~@(realize-tags c p v)))) :postwalk))

(defn- fig-product-tag
  "Lookup the tag value with the fig-product that is store in the
  configuration map under the namespaced ::fig-product keyword."
  [v]
  (Tag. (fn [c _] (get-in c [::fig-product v])) :postwalk))

(defn- fig-ref-internal*
  [v c p]
  (get-in p (realize-tags c p v)))

(def fig-ref-internal (memoize fig-ref-internal*))

(defn- fig-ref
  "Get reference to other parts of the configuration.

  Each ref is replaced with a Reference Record containing an anonymous
  function which takes the realized configuration.  The function runs get-in
  using the value of the ref tag on the configuration. The configuration map
  is then walked until there are no more Reference records left."
  [v]
  (Tag. (partial fig-ref-internal v) :postwalk))

(defn- fig-each-tag
  "For each item in the first collection, merge it with the rest of the items."
  [v]
  (Tag. (fn [c p]
          (let [[d & coll] (realize-tags c p v)
                cl (apply merge coll)]
            (map #(merge cl %) d)))
        :postwalk))

(def
  ^{:private true
    :doc "Holds the tag -> function mappings."}
  readers (atom
              {'fig/bigdec      fig-bigdec
               'fig/bool        fig-bool
               'fig/float       fig-float
               'fig/include     fig-include
               'fig/int         fig-int
               'fig/join        fig-join
               'fig/keyword     fig-keyword
               'fig/merge       fig-merge
               'fig/merge-each  fig-each-tag
               'fig/merge-into  fig-merge-into
               'fig/or          fig-or
               'fig/product     fig-product
               'fig/product-tag fig-product-tag
               'fig/ref         fig-ref}))

(defn add-reader!
  "Define a new reader function. Takes the tag name and an object that
  implements the Transformable Protocol."
  [s f]
  (swap! readers assoc s f))

(defn- reader
  "A function that takes a configuration and a file and runs the default tags
  for reading edn."
  [f]
  (try
    (with-open [r (-> f io/reader clojure.lang.LineNumberingPushbackReader.)]
      (try
        (edn/read {:readers @readers :eof nil} r)
        (catch java.lang.RuntimeException e
          (-> "Error while Reading Configuration File: %s"
              (format (.getMessage e))
              (ex-info {:file f
                        :line-number (.getLineNumber r)
                        :colum-number (.getColumnNumber r)} e)
              (throw)))))
    (catch java.io.FileNotFoundException e
      (-> (format "There was a problem reading the configuration file! %s" f)
          (ex-info {:filename f} e)
          (throw)))))

(defn read-config
  "Read the supplied configuration file. Argument is any value supported by
  clojure.java.io/reader"
  [f]
  (let [config (reader f)]
    (realize-tags {:path (.getParent (file f))} config config)))
