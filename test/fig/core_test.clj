(ns fig.core-test
  (:require [clojure.test :refer :all]
            [fig.core :refer :all]))

(deftest file-read-test
  (testing "Check if reading a config file succeeds"
    (is
     (= "config" (get (read-config "test/fig/simple-config.edn") :test)))))

(deftest empty-file-test
  (testing "Check if reading an empty config file succeeds"
    (is (nil? (read-config "test/fig/empty-config.edn")))))

(deftest join-tag-test
  (testing "Test the join tag"
    (is
     (= "STR1 STR2" (get-in (read-config "test/fig/tags-config.edn") [:join])))))

(deftest include-tag-test
  (testing "Test the include tag"
    (is
     (= "config" (get-in (read-config "test/fig/tags-config.edn") [:include :test])))))
